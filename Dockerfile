#See https://aka.ms/containerfastmode to understand how Visual Studio uses this Dockerfile to build your images for faster debugging.

FROM mcr.microsoft.com/dotnet/aspnet:6.0 AS base
ENV ASPNETCORE_ENVIRONMENT=Development
WORKDIR /app
EXPOSE 80

FROM mcr.microsoft.com/dotnet/sdk:6.0 AS build
WORKDIR /src
COPY ["EncryptDecryptBase64/Api/Api.csproj", "EncryptDecryptBase64/Api/"]
COPY ["EncryptDecryptBase64/EncryptDecryptBase64/EncryptDecryptBase64.csproj", "EncryptDecryptBase64/EncryptDecryptBase64/"]
RUN dotnet restore "EncryptDecryptBase64/Api/Api.csproj"
COPY . .
WORKDIR "/src/EncryptDecryptBase64/Api"
RUN dotnet build "Api.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "Api.csproj" -c Release -o /app/publish

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "Api.dll"]
