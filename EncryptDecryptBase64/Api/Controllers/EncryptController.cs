namespace Api.Controllers
{
    using Microsoft.AspNetCore.Mvc;
    using EncryptDecryptBase64;
    
    [Route("/api/v1/encryption/base64/[controller]")]
    [ApiController]
    public class EncryptController : ControllerBase
    {
        [HttpGet("{stringToEncrypt}", Name = "Encrypt")]
        public IActionResult EncryptString(string stringToEncrypt)
        {
            EncryptDecrypt base64Var = new EncryptDecrypt();
            string encryptedWord = base64Var.Encrypt(stringToEncrypt);
            return Ok(encryptedWord);
        }
    }
}