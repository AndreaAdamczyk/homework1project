namespace Api.Controllers
{
    using Microsoft.AspNetCore.Mvc;
    using EncryptDecryptBase64;
    [Route("/api/v1/encryption/base64/[controller]")]
    [ApiController]
    public class DecryptController : ControllerBase
    {
        [HttpGet("{stringToDecrypt}", Name = "Decrypt")]
        public IActionResult EncryptString(string stringToDecrypt)
        {
            EncryptDecrypt base64Var = new EncryptDecrypt();
            string decryptedWord = base64Var.Decript(stringToDecrypt);
            return Ok(decryptedWord);
        }
    }
}