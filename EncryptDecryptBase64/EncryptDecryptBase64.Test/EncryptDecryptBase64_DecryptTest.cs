﻿// <copyright file="EncryptDecryptBase64_DecryptTest.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace EncryptDecryptBase64.Test
{
    using System;
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    /// <summary>
    /// EncryptDecryptBase64_DecryptTest class.
    /// </summary>
    [TestClass]
    public class EncryptDecryptBase64_DecryptTest
    {
        private readonly EncryptDecrypt base64Var;

        /// <summary>
        /// Initializes a new instance of the <see cref="EncryptDecryptBase64_DecryptTest"/> class.
        /// </summary>
        public EncryptDecryptBase64_DecryptTest()
        {
            this.base64Var = new EncryptDecrypt();
        }

        /// <summary>
        /// Test.
        /// </summary>
        [TestMethod]
        public void Decrypting()
        {
            string text = "Hello";
            byte[] bytes = System.Text.UnicodeEncoding.UTF8.GetBytes(text);
            string encrypted = Convert.ToBase64String(bytes);
            string result = this.base64Var.Decript(encrypted);
            Assert.AreEqual(result, text);
        }
    }
}