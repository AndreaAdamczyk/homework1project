﻿// <copyright file="EncryptDecryptBase64_EncryptTest.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

/// <summary>
/// EncryptDecryptBase64 test.
/// </summary>
namespace EncryptDecryptBase64.Test
{
    using System;
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    /// <summary>
    /// Test class.
    /// </summary>
    [TestClass]
    public class EncryptDecryptBase64_EncryptTest
    {
        private readonly EncryptDecrypt base64;

        /// <summary>
        /// Initializes a new instance of the <see cref="EncryptDecryptBase64_EncryptTest"/> class.
        /// </summary>
        public EncryptDecryptBase64_EncryptTest()
        {
            this.base64 = new EncryptDecrypt();
        }

        /// <summary>
        /// Ecrypting test.
        /// </summary>
        [TestMethod]
        public void Encrypting()
        {
            string text = "Hello";
            string result = this.base64.Encrypt(text);
            byte[] bytes = System.Text.UnicodeEncoding.UTF8.GetBytes(text);
            string encrypted = Convert.ToBase64String(bytes);

            Assert.AreEqual(result, encrypted);
        }
    }
}
