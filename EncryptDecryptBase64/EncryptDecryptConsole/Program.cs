﻿// <copyright file="Program.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace EncryptDecryptConsole
{
    using System;
    using EncryptDecryptBase64;
    using MatthiWare.CommandLine;
    using MatthiWare.CommandLine.Core.Attributes;

    /// <summary>
    /// Program.
    /// </summary>
    class Program
    {
        static void Main(string[] args)
        {
            var options = new CommandLineParserOptions
            {
                AppName = "Encrypt Word",
            };
            CommandLineParser<ProgramOptions> parser = new CommandLineParser<ProgramOptions>(options);

            var result = parser.Parse(args);

            if (result.HasErrors)
            {
                Console.Error.WriteLine("Parsing has errors");
                return;
            }

            var programOptions = result.Result;

            EncryptDecrypt base64Var = new EncryptDecrypt();

            if (programOptions.Word != null)
            {
                string word = programOptions.Word;
                string encryptedWord = base64Var.Encrypt(word);

                Console.WriteLine($"The encrypted word is: {encryptedWord}");
            }

            if (programOptions.WordDecrypt != null)
            {
                string word2 = programOptions.WordDecrypt;
                string dencryptedWord = base64Var.Decript(word2);

                Console.WriteLine($"The decrypted word is: {dencryptedWord}");
            }
        }
    }

    /// <summary>
    /// Program options.
    /// </summary>
    public class ProgramOptions
    {
        [Name("w", "word")]
        [Description("Word to be encrypted")]
        public string Word { get; set; }

        /// <summary>
        /// Gets or sets the word.
        /// </summary>
        [Name("w2", "word2")]
        [Description("Word to be decrypted")]
        public string WordDecrypt { get; set; }
    }
}
