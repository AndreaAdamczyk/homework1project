﻿// <summary>
// EncryptDecryptBase64 class.
// </summary>
// <copyright file="EncryptDecrypt.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
namespace EncryptDecryptBase64
{
    using System;

    /// <summary>
    /// Class for encryption and decryption.
    /// </summary>
    public class EncryptDecrypt
    {
        /// <summary>
        /// Encrypt method.
        /// </summary>
        /// <param name="value">string.</param>
        /// <returns>result.</returns>
        public string Encrypt(string value)
        {
            string result = string.Empty;
            if (!string.IsNullOrEmpty(value))
            {
                byte[] bytes = System.Text.Encoding.UTF8.GetBytes(value);
                result = Convert.ToBase64String(bytes);
            }

            return result;
        }

        /// <summary>
        /// Decrypt method.
        /// </summary>
        /// <param name="value">string.</param>
        /// <returns>value.</returns>
        public string Decript(string value)
        {
            string result;
            byte[] bytes = Convert.FromBase64String(value);
            result = System.Text.Encoding.UTF8.GetString(bytes);
            return result;
        }

        /// <summary>
        /// Hello World.
        /// </summary>
        /// <returns>string.</returns>
        public string HelloWorld()
        {
            string helloWorld = "HelloWorld";
            return helloWorld;
        }
    }
}
