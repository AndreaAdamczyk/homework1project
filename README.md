# Homework1Project

Create a Dynamic Class Library using C# to solve the next problem:

● A method to encrypt an string to Base64
● A method to decrypt a Base64 string

Create a UnitTest Library (You can use MSTest, XUnit or NUnit) to test the functionality of the Task 1 (if you want to follow TDD you can start with Task 2 first)
