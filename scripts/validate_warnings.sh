COVERAGE_REPORT_PATH="./reportwarning.txt"

# Find number of current warnings
WARNINGS=$(grep -rnw $COVERAGE_REPORT_PATH -e 'Warning(s)') 
NUM_WARNINGS_PARTIAL=$( echo $WARNINGS | cut -d ":" -f2)
NUM_WARNINGS=$( echo $NUM_WARNINGS_PARTIAL | sed 's/[^0-9]*//g' )

# Load maximun warnings from Gitlab script
MAX_ALLOWED=$(echo "$(($MAX_WARNINGS + 0))")
#MAX_ALLOWED=$MAX_ALLOWED_WARNINGS 
#echo "$(($MAX_ALLOWED_WARNINGS + 0))"
#MAX_ALLOWED="100"

# Print current configuration
echo "WARNINGS : " $WARNINGS
echo "NUM MAX ALLOWED WARNINGS : " $MAX_ALLOWED
echo "NUM WARNINGS : " $NUM_WARNINGS

# Validate maximun warnings
if [ "$NUM_WARNINGS" -le "$MAX_ALLOWED" ];
then
    echo "Pass Successful"
    exit 0
fi

echo "Blocked. There are too many errors!!!"
exit 1