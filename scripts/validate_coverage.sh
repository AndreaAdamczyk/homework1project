#!/bin/bash

echo "Check Coverage"

COVERAGE_REPORT_PATH="./EncryptDecryptBase64/EncryptDecryptBase64.Test/reports/coverage.cobertura.xml"

RATE_REGEX='[0-1]\d*(\.\d+)?'

RATE=$( awk 'NR==2' $COVERAGE_REPORT_PATH | grep -Eio $RATE_REGEX | sed -n '1p')

# Represents 100% of coverage
MAX_COVERAGE="1"

if [ "$RATE" = "$MAX_COVERAGE" ];
then
    echo "Pass Successful"
    exit 0
fi

COVERAGE_VALUE=$( echo $RATE | tr "." "\n" | sed -n '2p' | cut -c1-1 )

echo $1
# Represents 90% of coverage
MIN_COVERAGE=$1
echo "RATE value " $RATE
echo "Coverage value " $COVERAGE_VALUE
echo "Min coverage " $MIN_COVERAGE

COVERAGE_VALUE=$( echo $RATE | tr "." "\n" | sed -n '2p' | cut -c1-1 )

if [ "$COVERAGE_VALUE" -ge "$MIN_COVERAGE" ];
then
    echo "Pass Successful"
    exit 0
fi

echo "Fail Coverage Check!"

# exit 1
